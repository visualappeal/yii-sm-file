<?php 

class FolderBreadcrumbs extends EBootstrapBreadcrumbs {
	public $folder_id = 0;
	
	public $file = null;
	
	private function addFolder($folder, &$folders, $url = true) {
		$title = $folder->title;
		
		if (is_numeric($title)) {
			$title = $title . ' ';
		}
		
		if ($url)
			$folders = $folders + array(
				$title => array('/file/folder/view', 'id' => $folder->id),
			);
		else
			$folders = $folders + array(
				$title,
			);
	}
	
	/*
	 * Get folders and init widget
	 */
	public function init() {
		EBootstrap::mergeClass($this->htmlOptions, array('file-breadcrumb'));
		
		$folders = array();
		
		$this->homeLink = EBootstrap::link(Yii::t('Folder', 'Schreibtisch'), array('/file/folder/index'));
		
		if ($this->folder_id > 0) {
			$folder = Folder::model()->findByPk($this->folder_id);
			
			if (!is_null($this->file)) {
				$folders = $folders + array(
					$this->file->fullFilename,
				);
				
				$this->addFolder($folder, $folders, true);
			}
			else
				$this->addFolder($folder, $folders, false);
			
		
			while (isset($folder->theParent->id)) {
				$folder = $folder->theParent;
				
				$this->addFolder($folder, $folders);
			}
		}
		
		$folders = array_reverse($folders);
		$this->links = $folders;
		
		parent::init();
	}
}

?>