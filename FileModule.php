<?php

class FileModule extends CWebModule
{
	/**
	 * Init application.
	 *
	 * @access protected
	 * @return void
	 */
	protected function init()
	{
		Yii::import('application.modules.file.models.*');
		Yii::import('application.modules.file.components.*');
		Yii::import('application.modules.file.widgets.*');

		return parent::init();
	}
}