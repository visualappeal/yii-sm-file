<?php

/*
 * https://github.com/valums/file-uploader
 */

/**
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr {
	private $temp = null;
	public $tempFile = '';
	private $realSize = 0;
	
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    public function save($path) {    
        if ($this->realSize != $this->getSize()){            
            return false;
        }
        
        $target = fopen($path, "w");        
        fseek($this->temp, 0, SEEK_SET);
        stream_copy_to_stream($this->temp, $target);
        fclose($target);
        
        return true;
    }
    
    public function getName() {
        return $_GET['qqfile'];
    }
    
    public function getMd5() {
    	return md5_file($this->tempFile);
    }
    
    public function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];            
        } else {
            throw new Exception('Getting content length is not supported.');
        }      
    }
    
    public function __construct() {
    	$input = fopen("php://input", "r");
        
        $this->tempFile = tempnam(sys_get_temp_dir(), 'qqu');
        $this->temp = fopen($this->tempFile, 'w+');
        $this->realSize = stream_copy_to_stream($input, $this->temp);
        
        fclose($input);
    }
    
    public function __destruct() {
    	if (!is_null($this->temp))
	    	fclose($this->temp);
    }
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm {  
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
            return false;
        }
        return true;
    }
    
    function getName() {
        return $_FILES['qqfile']['name'];
    }
    
    function getSize() {
        return $_FILES['qqfile']['size'];
    }
    
    function getMd5() {
    	return md5_file($_FILES['qqfile']['tmp_name']);
    }
}

class qqFileUploader {
    private $allowedExtensions = array();
    private $sizeLimit = 10485760;
    private $file;

    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){        
        $allowedExtensions = array_map("strtolower", $allowedExtensions);
            
        $this->allowedExtensions = $allowedExtensions;        
        $this->sizeLimit = $sizeLimit;
        
        $this->checkServerSettings();       

        if (isset($_GET['qqfile'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false; 
        }
    }
    
    private function checkServerSettings(){        
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));        
        
        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';             
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");    
        }        
    }
    
    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;        
        }
        return $val;
    }
    
    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     *
     * @param object $model File model
     */
    function handleUpload($model) {
    	$fileName = $this->file->getMd5();
		$path = Yii::app()->basePath.'/files/';
		$filePath = $path.$fileName;
		
		$size = $this->file->getSize();
		
		if (!$this->file){
            return array('error' => Yii::t('File', 'Es wurden keine Dateien zum hochladen gefunden!'));
        }
        
        if ($size == 0) {
            return array('error' => Yii::t('File', 'Die Datei ist leer!'));
        }
        
        if ($size > $this->sizeLimit) {
            return array('error' => Yii::t('File', 'Die Datei ist zu groß'));
        }
		
		if (!file_exists($filePath)) {
			if (is_writable($path)) {
				if ($this->file->save($filePath)) {
			        $pathinfo = pathinfo($this->file->getName());
			        $finfo = new finfo;
			        $mime = $finfo->file($filePath, FILEINFO_MIME_TYPE);
					return array(
						'success' => true,
						'filename' => $pathinfo['filename'],
						'file_hash' => $fileName,
						'filesize' => $size,
						'extension' => $pathinfo['extension'],
						'mime' => $mime,
					);
				}
				else {
					return array('error', Yii::t('File', 'Die Datei `{tempfilepath}` konnte nicht unter `{filepath}` gespeichert werden. Bitte wende dich an den Administrator.', array('{tempfilepath}' => $this->file->tempFile, '{filepath}' => $filePath)));
				}
			}
			else {
				return array('error' => Yii::t('File', 'Das Verzeichnis `{path}` ist nicht beschreibbar. Bitte wende dich an den Administrator.', array('{path}' => $path)));
			}
		}
		else {
			return array('error' => Yii::t('File', 'Die Datei wurde schon einmal hochgeladen. Benutze die Suche um die Datei zu finden.'));
		}        
    }    
}

?>