<?php
$this->breadcrumbs=array(
	Yii::t('File', 'Dateien') => array('/file/folder/index'),
	Yii::t('File', 'Datei hochladen'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('File', 'Datei hochladen'),
		'icon' => 'upload',
	)
);

$this->widget('FolderBreadcrumbs', array(
	'folder_id' => $folder->id,
));

$this->renderPartial('_form', array(
	'model' => $model,
	'update' => false,
));

$this->endWidget();