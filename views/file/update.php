<?php
$this->breadcrumbs=array(
	Yii::t('Core', 'Verwaltung') => array('/admin/index'),
	Yii::t('File', 'Dateiverwaltung') => array('/file/folder/index'),
	$model->fullFilename => array('/file/file/view', 'id' => $model->id),
);
?>

<h2><?php echo Yii::t('File', 'Datei `{filename}` bearbeiten', array('{filename}' => $model->fullFilename)); ?></h2>

<?php 
$this->widget('FolderBreadcrumbs', array(
	'folder_id' => $folder->id,
));
?>

<?php 
$this->renderPartial('_form', array(
	'model' => $model,
	'update' => true,
));
?>