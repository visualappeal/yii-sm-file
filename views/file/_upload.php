<?php 
$jsFile = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.file.js').'/fileuploader.js');
Yii::app()->clientScript->registerScriptFile($jsFile); 

$template = '<div class="qq-uploader">' .
                  	'<div class="qq-upload-drop-area"><p>'.Yii::t('File', 'Dateien hier hin bewegen').'</p>' . 
                  		'<p>'.Yii::t('File', 'oder').'</p>' . 
                  		'<a class="qq-upload-button btn btn-info"><i class="icon icon-upload icon-white"></i> '.Yii::t('File', 'Dateien auswählen').'</a>' . 
                	'</div>' .
               		'<ul class="qq-upload-list unstyled"> ' .
               		'</ul>' .
               		'<div class="clear"></div>' .
                '</div>';

$fileTemplate = '<li class="progress progress-info progress-striped">' .
    					'<div class="qq-upload-file-inner bar">' . 
			                '<div class="pull-left">' .
			                	'<span class="qq-upload-file"></span>' .
			                '</div>' .
			                '<div class="pull-right">' . 
			                	'<span class="qq-upload-size"></span> ' .
			                	'<a class="qq-upload-cancel" href="#"></a>' .
			                	'<span class="qq-upload-failed-text">' . Yii::t('File', 'Fehler') . '</span>' . 
			                '</div>' .
			        	'</div>' .
		            '</li>';

$successMessage = '<p>'.EBootstrap::ilabel(Yii::t('File', 'Alle Dateien wurden hochgeladen'), 'info').'</p>';

Yii::app()->clientScript->registerScript('file-html5-upload', "
var numUploads = 0;
var innerWidth = $('#file-upload').width();
var uploader = new qq.FileUploader({
	template: '".$template."',
    fileTemplate: '".$fileTemplate."',
	element: document.getElementById('file-upload'),
	action: '".Yii::app()->createAbsoluteUrl('/file/file/upload', array('parent' => $folder_id))."',
	sizeLimit: 10485760, //10 MB
	onSubmit: function(id, fileName) {
		numUploads = numUploads + 1;
	},
	onProgress: function(id, fileName, loaded, total) {
		$('.qq-upload-list li:eq('+id+') .qq-upload-file-inner').css({
			'width':  Math.round((loaded/total)*innerWidth),
		});
	},
	onComplete: function(id, fileName, data) {
		numUploads = numUploads - 1;
		if (numUploads == 0) {
			$('#file-upload-status').html('".$successMessage."');
			
			window.setTimeout('window.location = window.location;', 3000);
		}
	},
	debug: false,
});", CClientScript::POS_READY);
?>

<div id="file-upload"></div>
<div id="file-upload-status"></div>