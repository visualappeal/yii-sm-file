<?php $form=$this->beginWidget('EBootstrapActiveForm', array(
	'id'=>'folder-form',
	'horizontal' => true,
	'htmlOptions' => array(
		'enctype'=>'multipart/form-data',
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
	<?php if (!$update): ?>
		<?php echo $form->beginControlGroup($model, 'file_data'); ?>
			<?php echo $form->labelEx($model,'file_data'); ?>
			<?php echo $form->beginControls($model, 'file_data'); ?>
				<?php echo $form->fileField($model,'file_data',array('maxlength'=>100)); ?>
			<?php echo $form->endControls($model, 'file_data'); ?>
		<?php echo $form->endControlGroup($model, 'file_data'); ?>
	<?php else: ?>
		<?php echo $form->beginControlGroup($model, 'filename'); ?>
			<?php echo $form->labelEx($model,'filename'); ?>
			<?php echo $form->beginControls($model, 'filename'); ?>
				<?php echo $form->textField($model,'filename',array('maxlength'=>255, 'class' => 'span5')); ?>
			<?php echo $form->endControls($model, 'filename'); ?>
		<?php echo $form->endControlGroup($model, 'filename'); ?>
	<?php endif; ?> 
		
	<?php echo $form->beginControlGroup($model, 'description'); ?>
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->beginControls($model, 'description'); ?>
			<?php echo $form->textArea($model,'description', array('class' => 'span5', 'rows' => 8)); ?>
		<?php echo $form->endControls($model, 'description'); ?>
	<?php echo $form->endControlGroup($model, 'description'); ?>

	<?php echo $form->beginControlGroup($model, 'permission'); ?>
		<?php echo $form->labelEx($model,'permission'); ?>
		<?php echo $form->beginControls($model, 'permission'); ?>
			<?php echo $form->dropDownList($model, 'permission', $model->permissions); ?>
		<?php echo $form->endControls($model, 'permission'); ?>
	<?php echo $form->endControlGroup($model, 'permission'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo EBootstrap::submitButton($model->isNewRecord ? Yii::t('Project', 'Erstellen') : Yii::t('Project', 'Speichern'), 'success', '', false, 'upload', true); ?>
	<?php echo $form->endActions(); ?>

<?php $this->endWidget(); ?>