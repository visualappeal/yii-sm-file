<?php 
	Yii::app()->clientScript->registerScript('file-delete-confirm', '
	$("a.file-delete").live("click", function(e) {
		if (!confirm("'.Yii::t('File', 'Wollen Sie die Datei wirklich löschen?').'")) {
			e.preventDefault();
			return false;
		}
	});
	', CClientScript::POS_READY);
?>
<tr>
	<td class="file-icon"><span class="file-icon-default file-icon-<?php echo $data->extension; ?>"></span></td>
	<td class="file-toolbar">
		<a href="<?php echo $this->createUrl('/file/file/update', array('id' => $data->id)) ?>" title="<?php echo Yii::t('File', '{filename} bearbeiten', array('{filename}' => $data->fullFilename)) ?>"><i class="icon icon-pencil"></i></a>
		<a href="<?php echo $this->createUrl('/file/file/delete', array('id' => $data->id)) ?>" title="<?php echo Yii::t('File', '{filename} löschen', array('{filename}' => $data->fullFilename)) ?>" class="file-delete"><i class="icon icon-trash"></i></a>
	</td>
	<td class="file-title"><?php echo EBootstrap::link($data->fullFilename, array('/file/file/view', 'id' => $data->id)); ?></td>
</tr>