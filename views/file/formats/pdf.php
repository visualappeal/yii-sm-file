<object type="<?php echo $model->mime; ?>" data="<?php echo $this->createUrl('/file/download/src', array('id' => $model->id)); ?>#scrollbar=1&toolbar=1&statusbar=1&navpanes=1" width="700" height="600">
	<?php 
	echo Yii::t('File', 'Es ist kein PDF Plugin installiert. Du kannst die Datei aber {urlStart}herunterladen{urlEnd}', array(
		'{urlStart}' => '<a href="'.$this->createUrl('/file/download/src', array('id' => $model->id)).'">',
		'{urlEnd}' => '</a>',
	)); 
	?>
</object>