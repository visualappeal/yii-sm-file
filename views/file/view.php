<?php
$this->breadcrumbs=array(
	Yii::t('File', 'Dateien') => array('/file/folder/index'),
	$model->filename,
);

$filename = (empty($model->extension)) ? $model->filename : $model->filename.'.'.$model->extension;

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('File', 'Datei: {filename}', array('{filename}' => $filename)),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Folder', 'Ordner'), array('/file/folder/create', 'parent' => $model->theFolder->id), '', 'mini', false, 'plus', false, array('title' => Yii::t('Folder', 'Ordner erstellen'))),
			EBootstrap::ibutton(Yii::t('File', 'Datei'), array('/file/file/create', 'parent' => $model->theFolder->id), 'success', 'mini', false, 'upload', true, array('title' => Yii::t('File', 'Datei hochladen'))),
		),
		'icon' => 'folder-open',
	)
);

$this->widget('FolderBreadcrumbs', array(
	'folder_id' => $model->folder_id,
	'file' => $model,
));
?>

<table class="table table-stripped">
	<tbody>
		<tr>
			<td><?php echo Yii::t('File', 'Dateiname'); ?></td>
			<td><?php echo $filename; ?></td>
		</tr>
		<tr>
			<td><?php echo Yii::t('File', 'Größe'); ?></td>
			<td><?php echo $model->filesizeRead; ?></td>
		</tr>
		<?php if (!empty($model->description)): ?>
		<tr>
			<td><?php echo Yii::t('File', 'Beschreibung'); ?></td>
			<td><?php echo nl2br(strip_tags($model->description)); ?></td>
		</tr>
		<?php endif; ?>
		<tr>
			<td></td>
			<td><?php echo EBootstrap::ibutton(Yii::t('File', 'Download'), array('/file/download/download', 'id' => $model->id), '', '', false, 'download'); ?></td>
		</tr>
	</tbody>
</table>

<?php 
switch ($model->mime) {
	case 'application/pdf':
		$view = 'pdf'; break;
	case 'image/png':
	case 'image/svg+xml':
		$view = 'image'; break;
	default:
		$view = 'default'; break;
}

$this->renderPartial('formats/'.$view, array(
	'model' => $model,
));

$this->endWidget();