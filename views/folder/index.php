<?php
$this->breadcrumbs = array(
	Yii::t('File', 'Dateiverwaltung'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('File', 'Dateiverwaltung'),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Folder', 'Ordner'), array('/file/folder/create', 'parent' => 0), '', 'mini', false, 'plus', false, array('title' => Yii::t('Folder', 'Ordner erstellen'))),
		),
		'icon' => 'upload',
		'table' => false,
	)
);

$this->widget(
	'FolderBreadcrumbs', 
	array(
		'folder_id' => 0,
	)
);

$this->renderPartial(
	'/file/_upload', 
	array(
		'folder_id' => 0,
	)
);

if (count($folders)): 
?>
	<table id="files">
		<tbody>
			<?php
			foreach ($folders as $folder) {
				$this->renderPartial(
					'_view', 
					array(
						'data' => $folder,
					)
				);
			}
			?>
		</tbody>
	</table>
<?php 
else:
	echo EBootstrap::ilabel(Yii::t('Folder', 'Es sind noch keine Ordner vorhanden.'), 'info');
endif;

$this->endWidget();