<?php $form=$this->beginWidget('EBootstrapActiveForm', array(
	'id'=>'folder-form',
	'horizontal' => true,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->beginControlGroup($model, 'title'); ?>
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->beginControls($model, 'title'); ?>
			<?php echo $form->textField($model,'title'); ?>
		<?php echo $form->endControls($model, 'title'); ?>
	<?php echo $form->endControlGroup($model, 'title'); ?>
		
	<?php echo $form->beginControlGroup($model, 'parent'); ?>
		<?php echo $form->labelEx($model,'parent'); ?>
		<?php echo $form->beginControls($model, 'parent'); ?>
			<?php echo $form->textField($model,'parent', array('disabled' => true)); ?>
		<?php echo $form->endControls($model, 'parent'); ?>
	<?php echo $form->endControlGroup($model, 'parent'); ?>

	<?php echo $form->beginControlGroup($model, 'permission'); ?>
		<?php echo $form->labelEx($model,'permission'); ?>
		<?php echo $form->beginControls($model, 'permission'); ?>
			<?php echo $form->dropDownList($model, 'permission', $model->permissions); ?>
		<?php echo $form->endControls($model, 'permission'); ?>
	<?php echo $form->endControlGroup($model, 'permission'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo EBootstrap::submitButton($model->isNewRecord ? Yii::t('Project', 'Erstellen') : Yii::t('Project', 'Speichern'), 'success', '', false, $model->isNewRecord ? 'plus' : 'ok', true); ?>
	<?php echo $form->endActions(); ?>

<?php $this->endWidget(); ?>