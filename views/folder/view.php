<?php
$this->breadcrumbs=array(
	Yii::t('File', 'Dateiverwaltung') => array('/file/folder/index'),
	$model->title,
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => $model->title,
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Folder', 'Ordner'), array('/file/folder/create', 'parent' => $model->id), '', 'mini', false, 'plus', false, array('title' => Yii::t('Folder', 'Ordner erstellen'))),
			EBootstrap::ibutton(Yii::t('File', 'Datei'), array('/file/file/create', 'parent' => $model->id), 'success', 'mini', false, 'upload', true, array('title' => Yii::t('File', 'Datei hochladen'))),
		),
		'icon' => 'folder-open',
	)
);

$this->widget('FolderBreadcrumbs', array(
	'folder_id' => $model->id,
));

$this->renderPartial('/file/_upload', array(
	'folder_id' => $model->id,
));
?>

<table id="files">
	<tbody>
		<?php 
		foreach ($folders as $folder) {
			$this->renderPartial('_view', array(
				'data' => $folder,
			));
		}
		?>
		
		<?php 
		foreach ($files as $file) {
			$this->renderPartial('/file/_view', array(
				'data' => $file,
			));
		}
		?>
	</tbody>
</table>

<?php
$this->endWidget();