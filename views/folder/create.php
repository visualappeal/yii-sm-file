<?php
$this->breadcrumbs=array(
	Yii::t('File', 'Dateiverwaltung') => array('/file/folder/index'),
	Yii::t('Folder', 'Ordner Erstellen'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Folder', 'Ordner erstellen'),
		'icon' => 'folder-open',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model,
	)
);

$this->endWidget();