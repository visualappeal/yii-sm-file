<?php

/**
 * Download controller.
 *
 * @package File
 * @subpackage Download
 */
class DownloadController extends Controller
{
	/**
	 * Default view layout.
	 *
	 * @access public
	 * @var string
	 */
	public $layout = '//layouts/admin';
	
	/**
	 * Controller filters.
	 *
	 * @access public
	 * @return array
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	/**
	 * Controller access rules. Used by the accessControl filter.
	 *
	 * @access public
	 * @return array
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('download', 'src'),
				'roles' => array(User::LEVEL_MEMBER),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Download a file.
	 *
	 * @param integer $id File ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionDownload($id) {
		$model = File::model()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		$filename = Yii::app()->basePath.'/files/'.$model->file_hash;
		
		if (file_exists($filename)) {
			header('Content-type: '.$model->mime);
			header('Content-Disposition: attachment; filename="'.$model->filename.'"');
			readfile($filename);
		}
		else {
			Yii::app()->user->setFlash('error', Yii::t('File', 'Die Datei wurde gelöscht!'));
			$this->redirect(array('/file/file/view', 'id' => $model->id));
		}
	}
	
	/**
	 * Read file for HTML src attribute.
	 *
	 * @param integer $id File ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionSrc($id) {
		$model = File::model()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		$filename = Yii::app()->basePath.'/files/'.$model->file_hash;
		
		if (file_exists($filename)) {
			header('Content-type: '.$model->mime);
			readfile($filename);
			exit;
		}
		else {
			Yii::app()->user->setFlash('error', Yii::t('File.Download', 'Die Datei wurde schon gelöscht.'));
			$this->redirect(array('/file/file'));
		}
	}
}