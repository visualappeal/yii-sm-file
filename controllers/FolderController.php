<?php

/**
 * Folder controller.
 *
 * @package File
 * @subpackage Folder
 */
class FolderController extends Controller
{
	public $layout = '//layouts/admin';
	
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index', 'view', 'parent', 'create'),
				'roles' => array(User::LEVEL_MEMBER),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Display root folder.
	 *
	 * @access public
	 * @return void
	 */
	public function actionIndex() {
		$folders = Folder::model()->with('files')->findAllByAttributes(
			array(
				'parent_id' => 0,
			)
		);
		
		$this->render(
			'index', 
			array(
				'folders' => $folders,
			)
		);
	}
	
	/**
	 * Display folder.
	 *
	 * @param integer $id Parent folder ID
	 */
	public function actionView($id)
	{
		$model = Folder::model()->with('files')->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		$folders = Folder::model()->findAllByAttributes(
			array(
				'parent_id' => $model->id,
			)
		);
		
		$this->render(
			'view', 
			array(
				'model' => $model,
				'folders' => $folders,
				'files' => $model->files,
			)
		);
	}
	
	public function actionCreate($parent) {
		$model = new Folder('create');
		$model->parent_id = $parent;
		
		if (isset($_POST['Folder'])) {
			$model->attributes = $_POST['Folder'];
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('Folder', 'Ordner `{folder}` erstellt.', array('{folder}' => $model->title)));
				$this->redirect(array('/file/folder/view', 'id' => $model->id));
			}
		}
		else {
			$model->permission = $model->defaultPermission;
		}
		
		$this->render('create', array(
			'model' => $model,
		));
	}
}