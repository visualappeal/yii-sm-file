<?php

/**
 * File controller.
 *
 * @package File
 * @subpackage File
 */
class FileController extends Controller
{
	/**
	 * Default view layout.
	 *
	 * @access public
	 * @var string
	 */
	public $layout = '//layouts/admin';
	
	/**
	 * Controller filters.
	 *
	 * @access public
	 * @return array
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	/**
	 * Controller access rules. Used by the accessControl filter.
	 *
	 * @access public
	 * @return array
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('view', 'create', 'upload', 'update', 'delete'),
				'roles' => array(User::LEVEL_MEMBER),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Display a file.
	 *
	 * @param integer $id File ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionView($id)
	{
		$model = File::model()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		if (is_null($model->theFolder->id)) {
			Yii::app()->user->setFlash('error', Yii::t('File', 'Der Ordner wurde gelöscht. Bitte wende dich an einen Administrator'));
			throw new CHttpException(404);
		}
		
		$this->render('view', array(
			'model' => $model,
		));
	}
	
	/**
	 * Upload a new file via form model.
	 *
	 * @param integer $parent Parent folder ID (Default: 0)
	 *
	 * @access public
	 * @return void
	 */
	public function actionCreate($parent) {
		$parent = Folder::model()->findByPk($parent);
		
		if (is_null($parent))
			throw new CHttpException(404);
		
		$model = new File('create');
		$model->folder_id = $parent->id;
		
		if (isset($_POST['File'])) {
			$model->attributes = $_POST['File'];
			
			//Save file
			$model->file_data = CUploadedFile::getInstance($model, 'file_data');
			
			if (!is_null($model->file_data)) {
				$fileName = $model->generateFileName($model->file_data->extensionName);
				$path = Yii::app()->basePath.'/files/';
				$filePath = $path.$fileName;
				
				if (!file_exists($filePath)) {
					if (is_writable($path)) {
						$model->prepareFile();	
						if ($model->file_data->saveAs($filePath)) {							
							if ($model->save()) {
								Yii::app()->user->setFlash('success', Yii::t('File', 'Die Datei wurde hochgeladen.'));
								$this->redirect(array('/file/file/view', 'id' => $model->id));
							}
							else {
								Yii::app()->user->setFlash('error', Yii::t('File', 'Die Datei konnte hochgeladen aber nicht gespeichert werden.'));
								if (file_exists($filePath))
									unlink($filePath);
							}
						}
						else {
							Yii::app()->user->setFlash('error', Yii::t('File', 'Die Datei konnte nicht unter `{filepath}` gespeichert werden. Bitte wende dich an den Administrator.', array('{filepath}' => $filePath)));
						}
					}
					else {
						Yii::app()->user->setFlash('error', Yii::t('File', 'Das Verzeichnis `{path}` ist nicht beschreibbar. Bitte wende dich an den Administrator.', array('{path}' => $path)));
					}
				}
				else {
					$model->addError('file_data', Yii::t('File', 'Die Datei wurde schon einmal hochgeladen. Benutze die Suche um die Datei zu finden.'));
				}
			}
			else {
				$model->addError('file_data', Yii::t('File', 'Du musst eine Datei auswählen.'));
			}
		}
		else {
			$model->permission = $model->defaultPermission;
		}
		
		$this->render('create', array(
			'folder' => $parent,
			'model' => $model,
		));
	}
	
	/**
	 * Update file.
	 *
	 * @param integer $id File ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id) {
		$model = File::model()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);

		if (isset($_POST['File'])) {
			$model->attributes = $_POST['File'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('File', 'Die Datei wurde hochgeladen.'));
				$this->redirect(array('/file/file/view', 'id' => $model->id));
			}
		}
		else {
			$model->permission = $model->defaultPermission;
		}
		
		$this->render('update', array(
			'folder' => $model->theFolder,
			'model' => $model,
		));
	}
	
	/**
	 * Upload new file via ajax uploader.
	 *
	 * @param integer $parent Parent folder ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpload($parent) {
		$folder = Folder::model()->findByPk($parent);
		
		if (!is_null($folder)) {
			$model = new File('upload');
			$model->folder_id = $folder->id;
			
			$allowedExtensions = array();
			$sizeLimit = 10485760; //10 MB
			
			$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
			$result = $uploader->handleUpload($model);
			
			if (isset($result['success']) and ($result['success'] == true)) {
				$model->prepareFile($result);
				if (!$model->save()) {
					$filename = Yii::app()->basePath.'/files/'.$model->file_hash.'.'.$model->extension;
					if (file_exists($filename))
						unlink($filename);
					
					$result = array(
						'error' => Yii::t('File', 'Die Datei konnte hochgeladen aber nicht gespeichert werden!'),
					);
				}
			}
		}
		else {
			$result = array(
				'errror' => Yii::t('File', 'Der Ordner in dem die Datei gespeichert werden soll existiert nicht!'),
			);
		}

		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
	}
	
	/**
	 * Delete file.
	 *
	 * @param integer $id File ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id) {
		$model = File::model()->findByPk($id);
		
		if (is_null($model))
			throw new CHttpException(404);
		
		$filename = $model->fullFilename;
		$folder = Folder::model()->findByPk($model->folder_id);
		
		if ($model->delete()) {
			Yii::app()->user->setFlash('info', Yii::t('File', 'Die Datei `{filename}` wurde gelöscht.', array('{filename}' => $filename)));
			if (is_null($folder))
				$this->redirect(array('/file/folder/index'));
			else
				$this->redirect(array('/file/folder/view', 'id' => $folder->id));
		}
		else {
			throw new CHttpException(500);
		}
	}
}