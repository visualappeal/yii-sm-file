<?php

/**
 * Erste Migration um notwendige Tabellen zu erstellen.
 */
class m130823_082052_init extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'{{file_folder}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'title' => 'varchar(100) NOT NULL',
				'parent_id' => 'int(10) unsigned NOT NULL',
				'created' => 'datetime NOT NULL',
				'updated' => 'datetime NOT NULL',
				'permission' => 'int(10) unsigned NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `parent_id` (`parent_id`)',
			)
		);

		$this->createTable(
			'{{file_file}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'filename' => 'varchar(255) NOT NULL',
				'description' => 'text NOT NULL',
				'created' => 'datetime NOT NULL',
				'updated' => 'datetime NOT NULL',
				'folder_id' => 'int(10) unsigned NOT NULL',
				'downloads' => 'int(10) unsigned NOT NULL',
				'permission' => 'int(10) unsigned NOT NULL',
				'file_hash' => 'varchar(32) NOT NULL',
				'filesize' => 'int(10) unsigned NOT NULL',
				'mime' => 'varchar(50) NOT NULL',
				'extension' => 'varchar(20) NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `folder_id` (`folder_id`)',
			)
		);
	}

	public function safeDown()
	{
		$this->dropTable('{{file_file}}');
		$this->dropTable('{{file_folder}}');
	}
}