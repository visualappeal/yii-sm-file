<?php

/**
 * This is the model class for table "{{files}}".
 *
 * The followings are the available columns in table '{{files}}':
 * @property integer $id
 * @property integer $filename
 * @property string $description
 * @property string $created
 * @property string $updated
 * @property integer $folder_id
 * @property integer $downloads
 * @property integer $permission
 * @property string $file_hash
 * @property integer $filesize
 * @property string $mime
 * @property string $extension
 *
 * @package File
 * @subpackage File
 */
class File extends CActiveRecord
{
	/**
	 * Uploaded data
	 */
	public $file_data;
	
	/**
	 * Save attributes for afterDelete method
	 */
	private $_oldValues = array();
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Files the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{file_file}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('filename, file_hash, filesize, extension', 'required'),
			
			array('file_data, created', 'required', 'on' => 'create'),
			array('created', 'required', 'on' => 'upload'),
			array('updated', 'required', 'on' => 'update'),
			
			array('filesize, folder_id, downloads, permission', 'numerical', 'integerOnly' => true),
			
			array('filename', 'length', 'max' => 255),
			array('mime', 'length', 'max' => 50),
			array('extension', 'length', 'max' => 20),
			
			array('description', 'safe'),
			array('id, filename, file_hash, filesize, extension, mime, description, created, updated, folder_id, downloads, permission', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theFolder' => array(self::BELONGS_TO, 'Folder', 'folder_id'),
		);
	}
	
	public function defaultScope() {
		$t = $this->getTableAlias(false, false);
		return array(
			'condition' => "$t.permission <= :permission",
			'order' => "$t.filename ASC",
			'params' => array(
				':permission' => Yii::app()->user->level,
			),
		);
	}
	
	public function afterDelete() {
		$filename = Yii::app()->basePath.'/files/'.$this->_oldValues['file_hash'];
		
		if (is_file($filename))
			unlink($filename);
		
		return parent::afterDelete();
	}
	
	public function beforeValidate() {
		if ($this->isNewRecord)
			$this->created = date('Y-m-d H:i:s');
		else
			$this->updated = date('Y-m-d H:i:s');
		
		return parent::beforeValidate();
	}
	
	public function afterFind() {
		$this->_oldValues = $this->attributes;
		
		if (empty($this->created) or ($this->created == '0000-00-00 00:00:00'))
			$this->created = '';
		
		if (empty($this->updated) or ($this->updated == '0000-00-00 00:00:00'))
			$this->updated = '';
		
		return parent::afterFind();
	}
	
	/**
	 * Get all user permissions
	 */
	public function getPermissions() {
		return Yii::app()->user->getPermissions();
	}
	
	/**
	 * Get the default permission for a new subfolder
	 */
	public function getDefaultPermission() {
		if (isset($this->theFolder->permission))
			return $this->theFolder->permission;
		elseif (isset($this->folder_id) and ($this->folder_id > 0)) {
			$folder = Folder::model()->findByPk($this->folder_id);
			
			if (is_null($folder))
				return 0;
			else
				return $folder->permission;
		}
		else
			return 0;
	}
	
	/*
	 * Generate filename
	 */
	public function generateFileName($tempName = null) {
		return md5_file($this->file_data->tempName);
	}
	
	/**
	 * Get filename with extension
	 */
	public function getFullFilename() {
		if (!empty($this->extension))
			return $this->filename.'.'.$this->extension;
	}
	
	/**
	 * Get information about the uploaded file and save them into the database
	 */
	public function prepareFile($file = array()) {
		if (empty($file)) {
			$this->filename = $this->file_data->name;
			$this->file_hash = md5_file($this->file_data->tempName);
			$this->filesize = $this->file_data->size;
			$this->extension = $this->file_data->extensionName;
			$this->mime = $this->file_data->type;
		}
		else {
			$this->filename = $file['filename'];
			$this->file_hash = $file['file_hash'];
			$this->filesize = $file['filesize'];
			$this->extension = $file['extension'];
			$this->mime = $file['mime'];
		}
	}
	
	/**
	 * Get the filesize in a human readable form
	 */
	public function getFilesizeRead() {
		if (empty($this->filesize))
			$this->filesize = 0;
		if ($this->filesize > 1024*1024*1024) {
			return round($this->filesize/1024/1024/1024, 2).' GB';
		}
		elseif ($this->filesize > 1024*1024) {
			return round($this->filesize/1024/1024, 1).' MB';
		}
		elseif ($this->filesize > 1024) {
			return round($this->filesize/1024).' KB';
		}
		else {
			return $this->filesize.' Byte';
		}
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('File', 'ID'),
			'filename' => Yii::t('File', 'Dateiname'),
			'file_hash' => Yii::t('File', 'Hash'),
			'filesize' => Yii::t('File', 'Dateigröße'),
			'extension' => Yii::t('File', 'Dateiendung'),
			'mime' => Yii::t('File', 'Dateityp'),
			'description' => Yii::t('File', 'Beschreibung'),
			'created' => Yii::t('File', 'Erstellt'),
			'updated' => Yii::t('File', 'Bearbeitet'),
			'folder_id' => Yii::t('File', 'Ordner'),
			'downloads' => Yii::t('File', 'Downloads'),
			'permission' => Yii::t('File', 'Berechtigung'),
			'file_data' => Yii::t('File', 'Datei'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('filename',$this->filename);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('folder_id',$this->folder_id);
		$criteria->compare('downloads',$this->downloads);
		$criteria->compare('permission',$this->permission);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}