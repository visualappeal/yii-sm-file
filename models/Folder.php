<?php

/**
 * This is the model class for folders.
 *
 * The followings are the available columns in table '{{folders}}':
 * @property integer $id
 * @property string $title
 * @property integer $parent_id
 * @property string $created
 * @property string $updated
 * @property integer $permission
 *
 * @package File
 * @subpackage Folder
 */
class Folder extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @access public
	 * @return Folders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Get the table name for the model.
	 *
	 * @access public
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{file_folder}}';
	}

	/**
	 * Get the attribute validation rules.
	 *
	 * @access public
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('title, parent_id', 'required'),
			array('created', 'required', 'on' => 'create'),
			array('updated', 'required', 'on' => 'update'),
			array('parent_id, permission', 'numerical', 'integerOnly' => true),
			array('title', 'length', 'max' => 100),

			array('id, title, parent_id, created, updated, permission', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * Get folder relations.
	 *
	 * @access public
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theParent' => array(self::BELONGS_TO, 'Folder', 'parent_id'),
			'files' => array(self::HAS_MANY, 'File', 'folder_id'),
		);
	}
	
	/**
	 * Default folder scope.
	 *
	 * @access public
	 * @return array
	 */
	public function defaultScope() {
		$t = $this->getTableAlias(false, false);
		return array(
			'condition' => "$t.permission <= :permission",
			'params' => array(
				':permission' => Yii::app()->user->level,
			),
		);
	}
	
	/**
	 * Before validate method. Set created/updated date.
	 *
	 * @access public
	 * @return boolean
	 */
	public function beforeValidate() {
		if ($this->isNewRecord)
			$this->created = date('Y-m-d H:i:s');
		else
			$this->updated = date('Y-m-d H:i:s');
		
		return parent::beforeValidate();
	}
	
	/**
	 * After find method. Set date formats.
	 *
	 * @access public
	 * @return void
	 */
	public function afterFind() {
		if (empty($this->created) or ($this->created == '0000-00-00 00:00:00'))
			$this->created = '';
		
		if (empty($this->updated) or ($this->updated == '0000-00-00 00:00:00'))
			$this->updated = '';
		
		return parent::afterFind();
	}
	
	/**
	 * Get all user permissions
	 *
	 * @access public
	 * @return array
	 */
	public function getPermissions() {
		return Yii::app()->user->getPermissions();
	}
	
	/**
	 * Get the default permission for a new subfolder
	 *
	 * @access public
	 * @return integer
	 */
	public function getDefaultPermission() {
		if (isset($this->theParent->permission))
			return $this->theParent->permission;
		else
			return 0; 
	}
	
	/**
	 * Get parent folder.
	 *
	 * @access public
	 * @return string
	 */
	public function getParent() {
		if (isset($this->theParent->title))
			return $this->theParent->title;
		else {
			$parent = Folder::model()->findByPk($this->parent_id);
			
			if (is_null($parent))
				return Yii::t('Folder', 'Kein übergeordneter Ordner');
			else
				return $parent->title;
		}
	}

	/**
	 * Get attribute labels.
	 *
	 * @access public
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('Folder', 'ID'),
			'title' => Yii::t('Folder', 'Titel'),
			'parent' => Yii::t('Folder', 'Übergeordnet'),
			'parent_id' => Yii::t('Folder', 'Übergeordnet'),
			'created' => Yii::t('Folder', 'Erstellt'),
			'updated' => Yii::t('Folder', 'Bearbeitet'),
			'permission' => Yii::t('Folder', 'Berechtigung'),
		);
	}
}